/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/stm32f10x_it.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTI
  
  AL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_it.h"
uint16_t rx_timeout_count;
uint16_t tx_timeout_count;
/** @addtogroup STM32F10x_StdPeriph_Template
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
}
/******************************************************************************/
/*                 STM32F10x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f10x_xx.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/
void TIM2_IRQHandler(void)
{
	if ( TIM_GetITStatus(TIM2 , TIM_IT_Update) != RESET ) 
	{	
		gTimer--;
		TIM_ClearITPendingBit(TIM2 , TIM_FLAG_Update);  //清中断标志		 
	}		 	
}
void TIM3_IRQHandler(void)
{
	portBASE_TYPE xHigherPriorityTaskWoken = pdTRUE;
	if ( TIM_GetITStatus(TIM3 , TIM_IT_Update) != RESET ) 
	{	
		rx_timeout_count++;
		tx_timeout_count++;
		if(rx_timeout_count == 100 && e32_rx_buf_count !=0)//每100ms执行一次
		{
			xSemaphoreGiveFromISR(xSemaphore_rx,&xHigherPriorityTaskWoken);//二值信号量
			rx_timeout_count=0;
		}
		if(tx_timeout_count == 100 && new_msg_count !=0)//
		{
			xSemaphoreGiveFromISR(xSemaphore_DEBUG,&xHigherPriorityTaskWoken);//
			tx_timeout_count=0;
		}
		if(rx_timeout_count == 5000)//每5s执行一次
		{
			rx_timeout_count=0;
		}
		if(tx_timeout_count == 5000)//
		{
			tx_timeout_count=0;
		}
		TIM_ClearITPendingBit(TIM3 , TIM_FLAG_Update);  //	 
	}		 	
}
void USART1_IRQHandler(void)//debug发送接口
{
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
//		new_tx_msg[new_msg_count] = USART_ReceiveData(USART1);
//		new_msg_count++;
//		tx_timeout_count=0;
	}
	USART_ClearITPendingBit (USART1, USART_IT_RXNE);
}
void USART2_IRQHandler(void)//e32模块接口
{
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{	
		e32_rx_buf[e32_rx_buf_count] = USART_ReceiveData(USART2);
		e32_rx_buf_count++;
		rx_timeout_count=0;
	}
//	if(rx_buf_count > RF_MAX_PAYLOAD_SIZE-1) /*49为rx_buf的长度*/
//	{
//		xSemaphoreGiveFromISR(xSemaphore_rx,&xHigherPriorityTaskWoken);//二值信号量
//	}
	USART_ClearITPendingBit (USART2, USART_IT_RXNE);
}
void USART3_IRQHandler(void)
{
	uint8_t chTemp;
	if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
	{ 	
		chTemp = USART_ReceiveData(USART3);
		USART3_SendByte(chTemp);
	} 
	USART_ClearITPendingBit (USART3, USART_IT_RXNE);
}
/**
  * @}
  */ 


/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
