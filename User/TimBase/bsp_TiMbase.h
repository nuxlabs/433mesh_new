#ifndef TIME_TEST_H
#define TIME_TEST_H

#include "stm32f10x.h"

extern volatile u32 gTimer;

void TIM2_NVIC_Configuration(void);
void TIM2_Configuration(void);
void Delay_us(u32 nTimer);//ÿһ����ʱ10us
void Delay_ms(u32 nTimer);
#endif	/* TIME_TEST_H */
