#ifndef __MAIN_H
#define __MAIN_H

#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "stm32f10x.h"
#include "bsp_led.h"
#include "bsp_usart1.h"
#include "bsp_usart2.h"
#include "bsp_usart3.h"
#include "bsp_dht11.h"
#include "bsp_TiMbase.h"
#include "ebyte_e32_rf.h"
#include "pvcg_aodv.h"
#include "bsp_TiM3base.h"
#include "wdg.h"

#define MAX_NEW_MSG_LEN 128
#define MAX_MSG_LEN 128
#define MAX_RETRY 10 

//extern xQueueHandle MsgQueue_CreateMessage;
extern xSemaphoreHandle xSemaphore_rx;
extern xSemaphoreHandle xSemaphore_DEBUG;

extern uint8_t e32_rx_buf[128];
extern volatile uint16_t e32_rx_buf_count;
//extern uint16_t tx_buf_count;
extern volatile uint8_t new_msg_count;
extern uint8_t new_tx_msg[MAX_NEW_MSG_LEN];
#endif