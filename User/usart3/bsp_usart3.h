#ifndef __USART3_H
#define	__USART3_H

#include "stm32f10x.h"
#include <stdio.h>
#include <string.h>

void USART3_Config(void);

void USART3_SendByte(uint8_t ch);
void USART3_SendStr_length(uint8_t *str,uint32_t strlen);
void USART3_SendString(uint8_t *str);
#endif
