#ifndef __DHT11_H
#define	__DHT11_H

#include "stm32f10x.h"
#include "bsp_TiMbase.h"
#include "FreeRTOS.h"
#include "task.h"

#define HIGH  1
#define LOW   0

#define DHT11_CLK     RCC_APB2Periph_GPIOA
#define DHT11_PIN     GPIO_Pin_7                  
#define DHT11_PORT		GPIOA 

//带参宏，可以像内联函数一样使用,输出高电平或低电平
#define DHT11_DATA_OUT(a)	if (a)	\
					GPIO_SetBits(DHT11_PORT,DHT11_PIN);\
					else		\
					GPIO_ResetBits(DHT11_PORT,DHT11_PIN)
 //读取引脚的电平
#define  DHT11_DATA_IN()	   GPIO_ReadInputDataBit(DHT11_PORT,DHT11_PIN)

void DHT11_GPIO_Config(void);
static void DHT11_Mode_IPU(void);
static void DHT11_Mode_Out_PP(void);
uint8_t Read_TempHumi(uint8_t *TempHumi);
static uint8_t Read_Byte(void);

#endif /* __DHT11_H */







