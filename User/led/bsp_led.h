#ifndef __LED_H
#define	__LED_H

#include "stm32f10x.h"
#include "bsp_usart2.h"

#define IS_PB0_HIGH GPIO_ReadOutputDataBit(GPIOB, GPIO_Pin_0)
#define IS_PB1_HIGH GPIO_ReadOutputDataBit(GPIOB, GPIO_Pin_1)

#define Reset_PB0_1 GPIO_ResetBits(GPIOB, GPIO_Pin_0|GPIO_Pin_1) 

void LED_GPIO_Config(void);
void rf_get_node_addr_init();
#endif /* __LED_H */
