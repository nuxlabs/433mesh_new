/**
  ******************************************************************************
  * @file    bsp_led.c
  * @author  fire
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   led应用函数接口
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火 iSO-MINI STM32 开发板 
  * 论坛    :http://www.chuxue123.com
  * 淘宝    :http://firestm32.taobao.com
  *
  ******************************************************************************
  */
  
#include "bsp_led.h"   

 /**
  * @brief  初始化控制LED的IO
  * @param  无
  * @retval 无
  */
void LED_GPIO_Config(void)
{		
		/*定义一个GPIO_InitTypeDef类型的结构体*/
		GPIO_InitTypeDef GPIO_InitStructure;
		/*开启LED的外设时钟*/
		RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOB, ENABLE); 													   
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_5 | GPIO_Pin_6| GPIO_Pin_8;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;   /*通用推挽输出*/  
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
		GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13| GPIO_Pin_14| GPIO_Pin_15;
		GPIO_Init(GPIOB, &GPIO_InitStructure);
		  
		GPIO_ResetBits(GPIOA, GPIO_Pin_0|GPIO_Pin_5|GPIO_Pin_6);
		GPIO_ResetBits(GPIOB, GPIO_Pin_12| GPIO_Pin_13 | GPIO_Pin_14| GPIO_Pin_15);	
}
void rf_get_node_addr_init()
{
	GPIO_SetBits(GPIOA, GPIO_Pin_5|GPIO_Pin_6);
	Usart2_SendByte(0xc1);
	Usart2_SendByte(0xc1);
	Usart2_SendByte(0xc1);
}
