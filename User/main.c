/**
  ******************************************************************************
  * @file    main.c
  * @author  waro
  * @version V1.0
  * @date    2015-12-24
  * @brief   433M network using freeRTOSv8.2.3
  ******************************************************************************
  * @attention
  *
  * 实验平台:STM32f103c8
  * 
  ******************************************************************************
  */ 
#include "main.h"

uint8_t TempHumi[5];

xSemaphoreHandle xSemaphore_rx;
xSemaphoreHandle xSemaphore_CreateMessage;
xSemaphoreHandle xSemaphore_send;
xSemaphoreHandle xSemaphore_DEBUG;


uint8_t DEST_ADDR=0x01;//目的节点地址
uint8_t node_channel=0x17;//信道

//uint16_t tx_buf_count;

//关键数据是指：传感器的数据信息
//数据是指：包括关键信息在内的所有数据，还有源节点，目的节点，跳数等其他信息

uint8_t tx_msg[MAX_MSG_LEN];        //生成的关键数据的二级缓存
uint8_t new_tx_msg[MAX_NEW_MSG_LEN];//生成的关键数据一级缓存
volatile uint8_t new_msg_count = 0;          //生成的关键数据计数
uint8_t tx_buf[RF_MAX_PAYLOAD_SIZE];//发送的数据

uint8_t e32_rx_buf[128];            //e32模块串口接收数据一级缓存
volatile uint16_t e32_rx_buf_count;          //e32模块接收数据计数
uint8_t new_rx_buf[RF_MAX_PAYLOAD_SIZE];//设置为接收数据的二级缓存
uint8_t rxmsg[MAX_MSG_LEN];         //e32模块接收到的关键数据

uint8_t msg_seq_no = 0;
volatile uint8_t source_broadcasting = 0;
uint8_t retry = 0;

//TX Flags
AODV_RREQ_INFO* RREQ = NULL;
AODV_RREP_INFO* RACK = NULL;
AODV_RREP_INFO* RREP = NULL;
AODV_RERR_INFO* RERR = NULL;
AODV_MSG_INFO* RMSG = NULL;

void READ_ADDR_Task();
void RX_Task (void *data);
void TX_Task (void *data);
void createmessage();
void CreateMessage_Task (void *data);

/**
  * @brief  主函数
  * @param  无  
  * @retval 无
  */
int main(void)
{	
	xSemaphore_DEBUG = xSemaphoreCreateBinary();//二值信号量	
	xSemaphore_rx = xSemaphoreCreateBinary();//二值信号量	
	xSemaphore_CreateMessage =  xSemaphoreCreateBinary();//二值信号量
	xSemaphore_send = xSemaphoreCreateBinary();//二值信号量
	USART1_Config();
	USART2_Config();
	USART3_Config();
	LED_GPIO_Config();
	DHT11_GPIO_Config();
	TIM2_Configuration();
	TIM3_Configuration();
	IWDG_Init(5,625);//独立看门狗2s溢出
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 , ENABLE);
	rf_get_node_addr_init();

	xTaskCreate( READ_ADDR_Task, ( signed portCHAR *) "READ_ADDR_Task", configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY)+1, NULL);
	xTaskCreate( RX_Task, ( signed portCHAR *) "RX_Task", configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY), NULL);
	xTaskCreate( TX_Task, ( signed portCHAR *) "TX_Task", configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY), NULL);
	xTaskCreate( CreateMessage_Task, ( signed portCHAR *) "CreateMessage_Task", configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY), NULL);
	/* Start the scheduler. */
	vTaskStartScheduler();
}
void READ_ADDR_Task()
{
	while(1)
	{
		if( xSemaphoreTake( xSemaphore_rx, ( TickType_t ) 10 ) == pdTRUE )
		{
			if(e32_rx_buf[0]==0xc0&&e32_rx_buf[1]==0x00&&e32_rx_buf[3]==0x1a&&e32_rx_buf[4]==0x17&&e32_rx_buf[5]==0xc4)
			{
				node_addr=e32_rx_buf[2];
				printf("[READ_ADDR]node_addr is %d\n",node_addr);
			}
			else{
				printf("[READ_ADDR]the node_addr sets wrong\n");
			}
			memset(e32_rx_buf,0,e32_rx_buf_count);
			e32_rx_buf_count=0;
			GPIO_ResetBits(GPIOA, GPIO_Pin_5|GPIO_Pin_6);
			vTaskDelete(NULL);
		}
//		printf("[R-Addr]Task is not delete");
	}
}
void RX_Task (void *data)
{
	uint8_t i,len;
  uint8_t *local_rx_buf;
  uint8_t type,purpose;
  uint8_t next_hop;
  AODV_MSG_INFO aodvmsg;//route message
  AODV_RREQ_INFO aodvrreq;//route request
  AODV_RREP_INFO aodvrrep;//route response
  AODV_RREP_INFO aodvrack;//route acknowledge
  AODV_RERR_INFO aodvrerr;//route error
	
	rf_init (RF_MAX_PAYLOAD_SIZE, node_channel, node_addr);
	
	while (1)
	{
		if( xSemaphoreTake( xSemaphore_rx, ( TickType_t ) 10 ) == pdTRUE )//获取到二值信号量(即433模块串口收到数据)
		{
			memcpy( new_rx_buf, e32_rx_buf, e32_rx_buf_count);
			len = e32_rx_buf_count;
			e32_rx_buf_count=0;
			memset(e32_rx_buf,0,len);
			
			if (rf_polling_rx_packet(new_rx_buf, len)!=1)continue;//
			local_rx_buf = new_rx_buf;
//      USART3_SendStr_length(local_rx_buf,len);
			
			// Get the aodv msg type, zeroth byte, rx_buf[0]
      type = get_msg_type(local_rx_buf);
			
			if (type == 0) { //normal msg
        memset(rxmsg, 0, MAX_MSG_LEN);
        unpack_aodv_msg (local_rx_buf, &aodvmsg, rxmsg);
        printf("[RX-RMSG] type = %d, src = %d, nexthop = %d, dest = %d,msg_purp = %d,sendm_addr =%d, msg_len = %d, msg is in uart3 print\r\n",
				aodvmsg.type, aodvmsg.src, aodvmsg.next_hop, aodvmsg.dest, aodvmsg.msg_purp, aodvmsg.sendm_addr, aodvmsg.msg_len);
        // this node is destination, so deal received packet
				if(table_size==0){ //the node is resetted
					add_routing_entry(aodvmsg.src, aodvmsg.sendm_addr, aodvmsg.msg_seq_no, 0); 
          print_routing_table();
				}
				if(aodvmsg.dest == node_addr){
					switch (aodvmsg.msg_purp){
						case 0x00:
							break;
						case 0x01:
							DEST_ADDR = aodvmsg.src;
							xSemaphoreGive( xSemaphore_CreateMessage );
							break;
						case 0x02:
							break;
						default:
							printf("[RX-RMSG] invalid aodvmsg purpose\n");
						}
          memset(rxmsg, 0, aodvmsg.msg_len);
        }
				else {
          // this node is not destination, so send it to neighbor
          if((next_hop = find_next_hop_by_hop_count(aodvmsg.dest)) != 0){
            printf("[RX-DATA] send msg to %d\r\n", next_hop);
            //repack_forward_msg(&aodvmsg, next_hop,node_addr);
						aodvmsg.sendm_addr = node_addr;
						aodvmsg.next_hop = next_hop;
            RMSG = &aodvmsg;
          }
					else {
            aodvrerr.type = 3;
						aodvrerr.src = aodvmsg.src;
						aodvrerr.dest = aodvmsg.dest;
						aodvrerr.err_node = node_addr;
            RERR = &aodvrerr;
          }
        }
			}
			else if(type == 1) { // RREQ
        unpack_aodv_rreq(local_rx_buf, &aodvrreq);
        if (check_rreq_is_valid(&aodvrreq) != -1) {
          printf("[RX-RREQ] type = %d, broadcast_id = %d, src = %d, src_seq_num = %d, dest = %d, dest_seq_num = %d, hop_count = %d, sender_addr = %d\r\n", 
				  aodvrreq.type, aodvrreq.broadcast_id, aodvrreq.src, aodvrreq.src_seq_num, aodvrreq.dest, aodvrreq.dest_seq_num, aodvrreq.hop_count, aodvrreq.sender_addr);
				
          printf("[RX-RREQ] check is valid - adding the request to routing entry\r\n");
					print_rreq_buffer();
					// create inverse routing entry
          add_routing_entry(aodvrreq.src, aodvrreq.sender_addr, aodvrreq.src_seq_num, aodvrreq.hop_count); 
          print_routing_table();
					// this node is the destination, or there are path to dest in its routing table, so RREP!
					if ((find_next_hop(aodvrreq.dest) != 0) || aodvrreq.dest == node_addr) {
            printf("[RX-RREQ] this node is either destination or neighbor of the destination.\r\n");

            if ((dest_seq_num < aodvrreq.dest_seq_num) || (aodvrreq.dest_seq_num == 0)) {
              // update destination sequence number
              dest_seq_num = aodvrreq.dest_seq_num;
            }
						printf("[RX-RREQ] constructing rrep for receieved rreq...\r\n");
            aodvrrep.type = 2;
            aodvrrep.src = aodvrreq.src;
            aodvrrep.dest = aodvrreq.dest;
            aodvrrep.dest_seq_num = ++dest_seq_num;
						if(aodvrreq.dest == node_addr)
							aodvrrep.hop_count = 1;
						else
							aodvrrep.hop_count = find_min_hop_count_to_dest(aodvrreq.dest) + 1;
						aodvrrep.sendp_addr = node_addr;
            RREQ = NULL;
            RREP = &aodvrrep;
          }
					else {
            printf("[RX-RREQ] increase hop count\r\n");
            aodvrreq.hop_count++;
            aodvrreq.sender_addr = node_addr;

            printf("[RX-RREQ] broadcast rreq message\r\n");
            RREQ = &aodvrreq;
          }
				}
        /*
        else {
           printf("[RX-RREQ] invalid rreq...\r\n");
           RREP = NULL;
           RREQ = NULL;
        }
        */
			}
			else if(type == 2) { // RREP
				unpack_aodv_rrep (local_rx_buf, &aodvrrep);
        printf("[RX-RREP] type = %d, src = %d, dest = %d, dest_seq_num = %d, hop_count = %d, lifespan = %d, send_addr = %d\r\n",
				aodvrrep.type, aodvrrep.src, aodvrrep.dest, aodvrrep.dest_seq_num, aodvrrep.hop_count, aodvrrep.lifespan, aodvrrep.sendp_addr);
				if ((dest_seq_num < aodvrrep.dest_seq_num) || (aodvrrep.dest_seq_num == 0)) {
          printf("[RX-RREP] check is valid - updating destination sequence number\r\n");
          // update the destination sequence number
          dest_seq_num = aodvrrep.dest_seq_num;
        }

        // Creating a new or replace existing routing entry from the rrep message
        printf("[RX-RREP] adding routing table entry\r\n");
        add_routing_entry(aodvrrep.dest, aodvrrep.sendp_addr, aodvrrep.dest_seq_num, aodvrrep.hop_count); 
        print_routing_table();
        
        if (node_addr == aodvrrep.src) {
          source_broadcasting = 0;
          RREP = NULL;
					printf("[RX-RREP] this node is aodvrrep src!\r\n");
        }
				else{
					printf("[RX-RREP] increase hop count\r\n");
          aodvrrep.hop_count++;
          aodvrrep.sendp_addr = node_addr;

          printf("[RX-RREP] sending to src rrep message\r\n");
					RREP = &aodvrrep;
				}
			}
			else if(type == 3) { // RERR
        unpack_aodv_rerr (local_rx_buf, &aodvrerr);
//        printf("[RX-RERR] type = %d, srcAddr = %d\r\n", aodvrerr.type, rfRxInfo.srcAddr);

        if (node_addr == aodvrerr.src) {
					delete_routing_entry_by_dest(aodvrerr.dest);
          RERR = NULL;
          printf("[TX-RMSG] broadcasting rreq...\r\n");
          // construct RREQ message
          aodvrreq.type = 1;
          aodvrreq.broadcast_id = 1;
          aodvrreq.src = node_addr;
          aodvrreq.src_seq_num = node_seq_num; 
          aodvrreq.dest = DEST_ADDR;
          aodvrreq.dest_seq_num = dest_seq_num;
          aodvrreq.sender_addr = node_addr;
          aodvrreq.hop_count = 1;
          // set flag for tx_task, so tx_task can broadcast!
          RREQ = &aodvrreq;
        }
        else {
          RERR = &aodvrerr;
        }
				delete_next_hop_is_errnode_routing_entry(aodvrerr.err_node);
        source_broadcasting = 0;
        
        //if (node_addr == SRC_ADDR) clean_routing_table();
      
      }
			else if(type == 4) { // RACK (special RREP)
        /*
        unpack_aodv_rrep (local_rx_buf, &aodvrack);
        printf("[RX-RACK] type = %d, src = %d, dest = %d, dest_seq_num = %d, hop_count = %d, lifespan = %d, send_addr = %d\r\n",
				aodvrack.type, aodvrack.src, aodvrack.dest, aodvrack.dest_seq_num, aodvrack.hop_count, aodvrack.lifespan, aodvrack.send_addr);
				
        //The RACK should always be one hop
        if (find_next_hop(aodvrack.src) == aodvrack.src) {
          update_routing_entry(aodvrreq.dest, aodvrreq.dest, aodvrreq.broadcast_id, aodvrreq.hop_count);
        }
        else {
          //For first time discovery
          add_routing_entry(aodvrreq.dest, aodvrreq.dest, aodvrreq.broadcast_id, aodvrreq.hop_count);
        } */
      }
			else {
				printf("[DEBUG] unknown type\r\n");
			}
			memset(new_rx_buf,0,len);
			len=0;
		}
		vTaskDelay( 700 / portTICK_RATE_MS );
	}
}

void TX_Task (void *data)
{
	AODV_MSG_INFO aodvmsg;
  AODV_RREQ_INFO aodvrreq;
  AODV_RREP_INFO aodvrrep;
  AODV_RREP_INFO aodvrack;
  AODV_RERR_INFO aodvrerr;
	
	uint8_t i, len;
	
	while (1) 
	{
		if(xSemaphoreTake(xSemaphore_send,( TickType_t ) 10 )==pdTRUE ){

      aodvmsg.msg_len = new_msg_count;
      memcpy(tx_msg, new_tx_msg, new_msg_count);
//			memset(new_tx_msg, 0, new_msg_count);
      new_msg_count = 0;
			aodvmsg.type = 0;
      aodvmsg.src = node_addr;
      aodvmsg.dest = DEST_ADDR;
      aodvmsg.msg_seq_no = msg_seq_no++;
			aodvmsg.msg_purp = 0;
			aodvmsg.sendm_addr = node_addr;
      aodvmsg.msg = tx_msg;
      RMSG = &aodvmsg;
		}
		if (RERR) {//route error
      printf("[TX-RERR] inside the condition.\r\n");
      aodvrerr = *RERR;
      len = pack_aodv_rerr(tx_buf, aodvrerr);
      // Keep sending until ACK received
      while (send_rerr(tx_buf, find_next_hop_by_hop_count(aodvrerr.src), len) != 1) {
        vTaskDelay( 1000 / portTICK_RATE_MS );
        printf("sending rerr...");
      }
      RERR = NULL;
    }
		if (RREP) {//route response
      printf("[TX-RREP] inside the condition.\r\n");
      if (node_addr == DEST_ADDR) {
        node_seq_num++;
      }
      aodvrrep = *RREP;
      len = pack_aodv_rrep(tx_buf, aodvrrep);
      // Keep sending until ACK received, in EBYTE case sending always return 1
      retry = 0;
      while (send_rrep(tx_buf, find_next_hop_by_hop_count(aodvrrep.src), len) != 1 && retry < MAX_RETRY) {
        vTaskDelay( 1000 / portTICK_RATE_MS );
        printf("[TX-RREP] sending rrep...\r\n");
        retry++;
      }
      if (retry == MAX_RETRY) {
        retry = 0;
        aodvrerr.type = 3;
				aodvrerr.src = aodvrrep.src;
				aodvrerr.dest = aodvrrep.dest;
				aodvrerr.err_node = node_addr;
        RERR = &aodvrerr;
      }
      else {
        printf("[TX-RREP] successfully sent\r\n");
      }
      
      RREP = NULL;
    }
		if (RMSG) {//route message
      aodvmsg = *RMSG;
      if (!source_broadcasting || node_addr != aodvmsg.src) {
        if((aodvmsg.next_hop = find_next_hop_by_hop_count(aodvmsg.dest)) != 0){
          len = pack_aodv_msg(tx_buf, aodvmsg);
          
          // Keep sending until ACK received
          retry = 0;
          while (send_packet(tx_buf, len) != 1 && retry < MAX_RETRY) {
            vTaskDelay( 1000 / portTICK_RATE_MS );
            printf("sending message...\r\n");
            retry++;
          }
          if (retry == MAX_RETRY) {
            retry = 0;
            // maximum retry has reached, routing entry not working
            if (delete_routing_entry(aodvmsg.dest, aodvmsg.next_hop) != 0) {
              if (node_addr != aodvmsg.src && node_addr != DEST_ADDR) {
                aodvrerr.type = 3;
								aodvrerr.src = aodvmsg.src;
								aodvrerr.dest = aodvmsg.dest;
								aodvrerr.err_node = node_addr;
                RERR = &aodvrerr;
                RREQ = NULL;
              }
              else if (node_addr == aodvmsg.src) {
                // look for alternative routes
                printf("[TX-RMSG] broadcasting rreq...\r\n");
                // construct RREQ message
                aodvrreq.type = 1;
                aodvrreq.broadcast_id = 1;
                aodvrreq.src = aodvmsg.src;
                aodvrreq.src_seq_num = 1; 
                aodvrreq.dest = aodvmsg.dest;
                aodvrreq.dest_seq_num = dest_seq_num;
                aodvrreq.sender_addr = node_addr;
                aodvrreq.hop_count = 1;
                // set flag for tx_task, so tx_task can broadcast!
                RREQ = &aodvrreq;
              }
            }
          }
          else {
            // Sent successfully before MAX_RETRY
						printf("[TX-RMSG] successfully sent\r\n");
            RMSG = NULL;
          }
        }
			else {
          printf("[TX-RMSG] broadcasting rreq...\r\n");
          // construct RREQ message
          aodvrreq.type = 1;
          aodvrreq.broadcast_id = 1;
          aodvrreq.src = aodvmsg.src;
          aodvrreq.src_seq_num = 1; 
          aodvrreq.dest = aodvmsg.dest;
          aodvrreq.dest_seq_num = dest_seq_num;
          aodvrreq.sender_addr = node_addr;
          aodvrreq.hop_count = 1;
          // set flag for tx_task, so tx_task can broadcast!
          RREQ = &aodvrreq;
       }
      }
    }
		if (RREQ) {//route request
      printf("[TX-RREQ] inside the condition.\r\n");
      aodvrreq = *RREQ;
			if (node_addr == aodvrreq.src) {
        node_seq_num++;
      }
      printf("[TX-RREQ] type = %d, broadcast_id = %d, src = %d, src_seq_num = %d, dest = %d, dest_seq_num = %d, sender_addr = %d, hop_count = %d\r\n",
			aodvrreq.type, aodvrreq.broadcast_id, aodvrreq.src, aodvrreq.src_seq_num, aodvrreq.dest, aodvrreq.dest_seq_num, aodvrreq.sender_addr, aodvrreq.hop_count);
      len = pack_aodv_rreq(tx_buf, aodvrreq);
      // Keep sending until RREP received
      if (node_addr == aodvrreq.src) {
				source_broadcasting = 1;
        while (source_broadcasting) {
          broadcast_rreq(tx_buf, len);
          vTaskDelay( 3500 / portTICK_RATE_MS );
          printf("[TX-RREQ] ACK not received rebroadcasting...\r\n");

          aodvrreq.broadcast_id++;
					repack_aodv_rreq_id(tx_buf,aodvrreq.broadcast_id);
        }
      }
      else {
        if (broadcast_rreq(tx_buf, len) != 1) {
          printf("[TX-RREQ] Forwarding broadcast rreq failed...\r\n");
        }
      }
      RREQ = NULL;
    }
		vTaskDelay( 700 / portTICK_RATE_MS );
	}
}
void CreateMessage_Task (void *data)
{
	uint8_t i,retry_c;
	while(1)
	{
		if(xSemaphoreTake(xSemaphore_CreateMessage,( TickType_t ) 10 )==pdTRUE ){
			while(retry_c < MAX_RETRY){
				if( Read_TempHumi(new_tx_msg)==SUCCESS){
					new_msg_count=5;
					printf("[CM-Task]Read tempHumi success\n");
					xSemaphoreGive( xSemaphore_send );
					break;
				}
				retry_c++;
			}
			if(retry_c >= MAX_RETRY){
				for(i=0;i<5;i++){
					new_tx_msg[i]=0xff;
				}
				retry_c = 0;
				new_msg_count=5;
				printf("[CM-Task]Read tempHumi failed\n");
				xSemaphoreGive( xSemaphore_send );
			}
//			Usart2_SendStr_length(new_tx_msg,5);
		}
		IWDG_Feed();
	  vTaskDelay(1000 / portTICK_RATE_MS );
	}
}