#ifndef TIME3_TEST_H
#define TIME3_TEST_H

#include "stm32f10x.h"

void TIM3_NVIC_Configuration(void);
void TIM3_Configuration(void);

#endif	/* TIME_TEST_H */
