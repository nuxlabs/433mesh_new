/*******************************************************************************************************
 * INSTRUCTIONS:                                                                                       *
 * Startup:                                                                                            *
 *     1. Create a EBYTE_E32_RF_RX_INFO structure, and initialize the following members:               *
 *         - rfRxInfo.pPayload (must point to an array of at least RF_MAX_PAYLOAD_SIZE bytes)          *
 *     2. Call rf_init() to initialize the packet protocol, address of the node and channel it uses    *
 *        are set in this call                                                                         *
 *                                                                                                     *
 * Transmission:                                                                                       *
 *     1. Create a EBYTE_E32_RF_TX_INFO structure, and initialize the following members:               *
 *         - rfTxInfo.destAddr (the destination address)                                               *
 *         - rfTxInfo.pPayload (the payload data to be transmitted to the other node)                  *
 *         - rfTxInfo.length (the size od rfTxInfo.pPayload)                                           *
 *     2. Call rf_tx_packet()                                                                          *
 *                                                                                                     *
 * Reception:                                                                                          *
 *     1. When data arrives at E32 serial interface, serial RX interrupt of the MCU sets rx_signal,    *
 *        which is waited by rf_polling_rx_packet()                                                    *
 *                                                                                                     *
 * FRAME FORMATS:                                                                                      *
 * Data packets:                                                                                       *
 *     [Node address (2)][Channel (1)][Payload (variable length)][Checksum (1)]                        *
 * Target platform: EBYTE_E32_TTL_100                    *
 *******************************************************************************************************/
#ifndef _EBYTE_E32_RF_H
#define _EBYTE_E32_RF_H

#include "stm32f10x.h"
#include "bsp_usart1.h"
#include "bsp_usart2.h"
#include "bsp_usart3.h"
#include <string.h>

//-------------------------------------------------------------------------------------------------------
// Constants concerned with the Basic RF packet format
// Packet overhead：2 bytes destination address, 1 byte channel id, 1 byte checksum
#define RF_PACKET_OVERHEAD_SIZE   2 + 1 + 1
#define RF_MAX_PAYLOAD_SIZE		(128 - RF_PACKET_OVERHEAD_SIZE)

//-------------------------------------------------------------------------------------------------------
// The data structure which is used to transmit packets
typedef struct {
  uint16_t destAddr;
  uint8_t length;
  uint8_t *pPayload;
} EBYTE_E32_RF_TX_INFO;

// The receive struct:
//typedef struct {
//  uint8_t length;
//  uint8_t max_length;
//  uint8_t *pPayload;
//} EBYTE_E32_RF_RX_INFO;

// The RF settings structure:
typedef struct {
	uint8_t max_legth;
  uint16_t myAddr;
	uint8_t channel_id;
} RF_SETTINGS;

extern volatile uint8_t node_addr;

//extern uint8_t e32_tx_buf[128];
extern EBYTE_E32_RF_TX_INFO rfTxInfo;

void rf_power_up();
void rf_power_down();
uint8_t rf_get_hw_addr();
uint8_t rf_tx_packet(EBYTE_E32_RF_TX_INFO *pRTI);
uint8_t rf_polling_rx_packet(uint8_t *,uint16_t e32_rx_buf_length);
//-------------------------------------------------------------------------------------------------------
//  void rf_init(RF_RX_INFO *pRRI, uint8_t channel, uint16_t myAddr)
//
//  DESCRIPTION:
//      Initializes EBYTE E32 for radio communication via the RF library functions. Powers on E32, writes 
//      all necessary configuration commands.
//
//  ARGUMENTS:
//      RF_RX_INFO *pRRI
//          A pointer the RF_RX_INFO data structure to be used during the first packet reception.
//			The structure can be switched upon packet reception.
//      uint8_t channel
//          The RF channel to be used (00H = 410MHz, ... 1FH = 441Mhz. 433MHz = 17H)
//      uint16_t myAddr
//          The 16-bit short address which is used by this node.
//-------------------------------------------------------------------------------------------------------
void rf_init(uint8_t max_length, uint8_t channel, uint16_t myAddr);

#endif
