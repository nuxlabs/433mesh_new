#ifndef _PVCG_AODV_H
#define _PVCG_AODV_H

// XL NOTE: put OS related headers here, all os_* calls in the package should be replaced 
/* TODO: OS related headers */
#include "stm32f10x.h"
#include "bsp_usart1.h"
#include "ebyte_e32_rf.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct _routing_entry{
  uint8_t dest;
  uint8_t next_hop;
  uint8_t hop_count;
  uint8_t dest_seq_num;
  uint8_t lifespan;
} ROUTING_ENTRY; 

typedef struct{
  uint8_t type;
  uint8_t src;
  uint8_t next_hop;
  uint8_t dest;
  uint8_t msg_seq_no;
	uint8_t msg_purp;
	uint8_t sendm_addr;
  uint8_t msg_len;
  uint8_t* msg;
} AODV_MSG_INFO;

typedef struct{
  uint8_t type;
  uint8_t broadcast_id;
  uint8_t src;
  uint8_t src_seq_num;
  uint8_t dest;
  uint8_t dest_seq_num;
  uint8_t sender_addr;
  uint8_t hop_count;
} AODV_RREQ_INFO;

// XL NOTE: removed ssnr2
typedef struct{
  uint8_t type;
  uint8_t src;
  uint8_t dest;
  uint8_t dest_seq_num;
  uint8_t hop_count;
  uint8_t lifespan;
	uint8_t sendp_addr;
} AODV_RREP_INFO;

typedef struct{
  uint8_t type;
	uint8_t src;
	uint8_t dest;
	uint8_t err_node;
} AODV_RERR_INFO;

// XL NOTE: changed nrk_time_t to os_time_t
extern uint8_t node_seq_num;
extern uint8_t dest_seq_num;
extern uint8_t table_size;
extern uint8_t rreq_buffer_size;
extern ROUTING_ENTRY routing_table[];
extern AODV_RREQ_INFO rreq_buffer[];

//extern uint8_t tx_buf[RF_MAX_PAYLOAD_SIZE];
//extern uint8_t rx_buf[RF_MAX_PAYLOAD_SIZE];

uint8_t get_msg_type(uint8_t* rx_buf);
uint8_t get_aodvmsg_purp(uint8_t *rx_buf);

void unpack_aodv_rreq(uint8_t* rx_buf, AODV_RREQ_INFO* aodvrreq);
void unpack_aodv_rrep(uint8_t* tx_buf, AODV_RREP_INFO* aodvrrep);
void unpack_aodv_msg(uint8_t* rx_buf, AODV_MSG_INFO* aodvmsg, uint8_t* msg);
void unpack_aodv_rerr(uint8_t* rx_buf, AODV_RERR_INFO* aodvrerr);

uint8_t pack_aodv_rreq(uint8_t* tx_buf, AODV_RREQ_INFO aodvrreq);
uint8_t pack_aodv_rrep(uint8_t* tx_buf, AODV_RREP_INFO aodvrrep);
uint8_t pack_aodv_msg(uint8_t* tx_buf, AODV_MSG_INFO aodvmsg);
uint8_t pack_aodv_rerr(uint8_t* tx_buf, AODV_RERR_INFO aodvrerr);

void repack_forward_msg(AODV_MSG_INFO* aodvmsg, uint8_t next_hop,uint8_t sendm_addr);
void repack_aodv_rreq_id(uint8_t *tx_buf,uint8_t broadcase_id);

int8_t add_routing_entry(uint8_t dest, uint8_t next_hop, uint8_t dest_seq_num, uint8_t hop_count);
int8_t delete_routing_entry(uint8_t dest, uint8_t next_hop);
int8_t delete_next_hop_is_errnode_routing_entry(uint8_t next_hop);
int8_t delete_routing_entry_by_dest(uint8_t dest);
void renew_routing_entry(uint8_t dest, uint8_t dest_seq_num);
int8_t clean_routing_table();
void print_routing_table();

uint8_t find_index(uint8_t dest, uint8_t dest_seq_num);
uint8_t find_next_hop(uint8_t dest);
uint8_t find_next_hop_by_hop_count(uint8_t dest);
uint8_t find_min_hop_count_to_dest(uint8_t dest);

uint8_t send_packet(uint8_t *tx_buf, uint8_t length);
uint8_t send_rrep(uint8_t *tx_buf, uint8_t next_hop, uint8_t length);
uint8_t broadcast_rreq(uint8_t *tx_buf, uint8_t length);
uint8_t send_rerr(uint8_t *tx_buf, uint8_t next_hop, uint8_t length);

int8_t add_rreq_to_buffer(AODV_RREQ_INFO* aodvrreq);
int8_t check_rreq_is_valid(AODV_RREQ_INFO* aodvrreq);
void print_rreq_buffer();

#endif
