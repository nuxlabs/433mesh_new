// XL NOTE: put OS related headers here, all os_* calls in the package should be replaced 
/* TODO: OS related headers */

#include "ebyte_e32_rf.h"

typedef struct _ebyte_e32_header{	
  uint16_t dest_addr;
  uint8_t channel_id;
} ebyte_e32_header_t;

volatile uint8_t node_addr;//此处为本节点地址

uint8_t e32_tx_buf[128];//e32模块发送的数据

EBYTE_E32_RF_TX_INFO rfTxInfo;

uint8_t rf_ready;
volatile RF_SETTINGS rfSettings;
volatile uint8_t rx_ready;
volatile uint8_t tx_done;

void rf_power_down()
{
  // XL NOTE: TODO - GPIO power down E32 VDD
}

void rf_power_up()
{

}

uint8_t rf_get_hw_addr()
{
  // XL NOTE: read out hardware address of E32 module
  // read each node_addr is realised in main.c
    return node_addr;
}

void rf_init(uint8_t max_length, uint8_t channel, uint16_t myAddr)
{ 
  /* Initialize settings struct */
	rfSettings.max_legth = max_length;
  rfSettings.myAddr = myAddr;
  rfSettings.channel_id = channel;

  rf_ready = 1;
  rx_ready = 0;
  tx_done = 0;
}

uint8_t rf_polling_rx_packet(uint8_t *new_rx_buf, uint16_t len)
{
	uint8_t checksum,i;
	
  if(!rf_ready) {
    printf("[RX-BASIC_RF] rf_ready is not ready\n");
    return 0;
  }

  if((len > rfSettings.max_legth) || (len < 3)){
    printf("[RX-BASIC_RF] frame size is incorrect\n");
    return 0;
  }
	if(new_rx_buf[len-1] != 0xff){
		printf("[RX-BASIC_RF] frame tail is incorrect !\n");
		return 0;
	}
	//XL: checksum 
//	for(i=0;i<len-2;i++)
//	{
//		checksum+=new_rx_buf[i];
//	}
//	if(checksum != now_rx_buf[len-2]){
//	  printf("[RX-BASIC_RF] frame checksum is incorrect !\n");
//    return 0;
//	}
	
//	Usart1_SendStr_length(rfSettings.pRxInfo->pPayload,rfSettings.pRxInfo->length);
  return 1;
}

uint8_t rf_tx_packet(EBYTE_E32_RF_TX_INFO *pRTI)
{
	uint8_t *frame_start = e32_tx_buf;//整个帧起始，指向发送的数据
  uint8_t *data_start;//数据帧起始(不包括MAC头和信道)
  uint8_t i;
  ebyte_e32_header_t *e32_head = (ebyte_e32_header_t * ) frame_start;
	
  if(!rf_ready) {
    printf("[RX-BASIC_RF] rf_ready is not ready\r\n");
    return 0;
  }

  /* Build the rest of the MAC header */
  e32_head->dest_addr = pRTI->destAddr<<8 | pRTI->destAddr >> 8;//小端存储
  e32_head->channel_id = rfSettings.channel_id;

  /* Copy data payload into packet */
  data_start = frame_start + sizeof(ebyte_e32_header_t) -1;//移动指针
  memcpy(data_start, pRTI->pPayload, pRTI->length);
  // XL NOTE: add checksum, for now always 0
  *(data_start + pRTI->length) = 0xff;
//	USART3_SendStr_length(e32_tx_buf,sizeof(ebyte_e32_header_t) + pRTI->length);
	Usart2_SendStr_length(e32_tx_buf,sizeof(ebyte_e32_header_t) + pRTI->length );//- 1 + 1);
  /* TODO */
  tx_done = 1;  
  return 1;
}
